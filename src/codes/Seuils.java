package codes;

/**
 * 
 */


/**
 * @author haddad
 *
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.lang.* ; 
import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.collections.list.TreeList;


import com.csvreader.CsvReader;

public class Seuils {  

	public static String  pathFile = "/home/haddad/workspaceJava/ProcessSimulationsAbcCedrus/" ;
	public static Map<Integer,ArrayList<String>> elementsMapStepToSimData           = new TreeMap<Integer,ArrayList<String>>()  ;
	public static Map<Integer,ArrayList<String>> elementsMapStepToSimDataDecoupe    = new TreeMap<Integer,ArrayList<String>>()  ;

	public static Map<Integer,ArrayList<String>> elementsMapStepToPodsData          = new TreeMap<Integer,ArrayList<String>>() ; 
	public static Map<Integer,ArrayList<String>> elementsMapStepToPodsDataDecoupe   = new TreeMap<Integer,ArrayList<String>>() ; 
	
	public static Map<Integer,Double> mapSSsimMoy = new TreeMap<Integer,Double>() ;

	public static List<Double>   poids  = new ArrayList<Double>() ;
	public static List<Double>   seuils = new ArrayList<Double>() ; 

	public static enum Append {
		OUI, NON ;
	} ; 
	//----------------------------------------------------------------------------------------------
	public static void initPoids () {
		for (int i = 0 ; i < 198 ; i++) { poids.add(1.0D) ;  }
	}
	//----------------------------------------------------------------------------------------------
	public static void initSeuils () {
		Double seuil ; 
		for (int i = 0 ; i < 100; i++) { 
			seuil = getSeuil(i+1,10000,0.001D) ;
			seuils.add(seuil) ;  
		}
	}
	//----------------------------------------------------------------------------------------------
	public static Double calculSSsimMoy(int k ) {
		int iSim = 1 ; 
		int    l = 1 ; 
		Double somme = 0D ; 
		int taille =  (elementsMapStepToSimDataDecoupe.keySet()).size() ;
		while ( (iSim <= taille)  && (l <= 1000 ) ) {
			ArrayList<String> elementsSimul = elementsMapStepToSimDataDecoupe.get(iSim) ; 
			if ( ! ((elementsSimul.get(k)).contentEquals("NAN") )) {
				String curValSim = elementsSimul.get(k) ; 
				somme = somme + (Double.valueOf(curValSim)) ; 
				l++ ; 
			}
			iSim++ ; 
		}
		return somme/1000  ; 
	}
	//----------------------------------------------------------------------------------------------
	public static void setMapSSimMoy() {
			for (int i = 0 ; i < poids.size() ; i++) {
				mapSSsimMoy.put(i,calculSSsimMoy(i)) ;
			}
	}
	//----------------------------------------------------------------------------------------------
	public static Double distanceSSobsSSsim(int iSim, int jPods ) {

		Double distance =  0.0D ; 
		Double trucSomme = 0.0D ;

		ArrayList<String> elementsPods  = elementsMapStepToPodsDataDecoupe.get(jPods)  ;
		ArrayList<String> elementsSimul = elementsMapStepToSimDataDecoupe.get(iSim)  ;

		int taillePoids = poids.size() ; 
		for (int k = 0 ; k < taillePoids ; k++ ) {

			String curValPods = elementsPods.get(k) ; 
			String curValSim  = elementsSimul.get(k) ;
			Double valPods    = 0.0D ;    
			Double valSim     = 0.0D ; 
			/*
			IF  (SSobs,k ==  NAN)   ALORS  truc = 0 
			SINON  IF  SSsim,K== NAN ALORS  calculer truc = ((SSobs,k -SSmoy,k)**2)/Vk 
                   SINON   calculer truc = ((SSobs,k -SSsim,k)**2)/Vk
			 */
			if (curValPods.contentEquals("NAN")) { trucSomme = 0D ; }
			else {
				valPods = Double.valueOf(curValPods);
				if (curValSim.contentEquals("NAN")) {
					valSim    = mapSSsimMoy.get(k)  ; 
					trucSomme = Math.pow((valPods - valSim),2) / poids.get(k) ; 
				} 
				else { 
					valSim    = Double.valueOf(curValSim) ; 
					trucSomme =  Math.pow((valPods - valSim),2) / poids.get(k) ; 
				} 
			}
			distance = distance + trucSomme   ; 
		}
		return distance ; 
	}
	//----------------------------------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public static Double getSeuil(int numPods, int nbrSimul, Double ratio) {
		TreeList   concat = new TreeList() ; 

		for (int iSimul = 1 ; iSimul <= nbrSimul ; iSimul++) {
			Double distance = distanceSSobsSSsim(iSimul, numPods) ; 
			concat.add(distance) ; 
		}
	
		Collections.sort(concat);	
		// System.out.println(concat) ; 
		int indiceSeuil = new Double(nbrSimul*ratio).intValue();
		return (Double) concat.get(indiceSeuil) ; 
	}
	
	//----------------------------------------------------------------------------------------------
	public static ArrayList<String> decoupePodsOrSimul(ArrayList<String> infosPodsOrSimul) {
		ArrayList<String> retValeurs = new ArrayList<String>()  ;  // paramètres + valeurs 

		// Paramètres (step, valeurs - 9 - )
		// retValeurs.addAll(infosPodsOrSimul.subList(1,10)) ; 
		// --------------------------------- Valeurs liées aux polygones
		retValeurs.addAll(infosPodsOrSimul.subList(12,30)) ; 
		retValeurs.addAll(infosPodsOrSimul.subList(32,50)) ;
		retValeurs.addAll(infosPodsOrSimul.subList(52,70)) ;
		retValeurs.addAll(infosPodsOrSimul.subList(72,90)) ;
		retValeurs.addAll(infosPodsOrSimul.subList(92,110)) ;
		retValeurs.addAll(infosPodsOrSimul.subList(112,130)) ;
		retValeurs.addAll(infosPodsOrSimul.subList(132,150)) ; 
		retValeurs.addAll(infosPodsOrSimul.subList(152,170)) ;
		retValeurs.addAll(infosPodsOrSimul.subList(172,190)) ;
		// ------------------ Valeurs  FST
		for (int i=0 ;  i< 106 ;  i=i+3 ) {
			retValeurs.addAll(infosPodsOrSimul.subList(193 + i, 193 + i +1)) ; 
		}
		return retValeurs ; 
	}
	//-------------------------------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public static void getPodsData() {
		try {
			CsvReader  productsPods        = new CsvReader(pathFile+"src/files/pods_resultats.csv",';');
			String[]   valuesPodsInLine        ;    
			Integer    stepPodsInCurRecord = 1 ;
			ArrayList<String> elementsPods ; 
			ArrayList<String> elementsPodsDecoupe ; 

			while ((productsPods.readRecord())) {
				stepPodsInCurRecord =  Integer.valueOf(productsPods.getValues()[0]) ; 
				elementsPods        =  new ArrayList<String>(Arrays.asList(productsPods.getValues())) ;
				if (elementsMapStepToPodsData.containsKey(stepPodsInCurRecord)) {
					elementsMapStepToPodsData.get(stepPodsInCurRecord).addAll(elementsPods); 
				}
				else { elementsMapStepToPodsData.put(stepPodsInCurRecord,elementsPods) ; }
			}
			productsPods.close() ;

			for (int ipods  : elementsMapStepToPodsData.keySet()) {
				elementsPodsDecoupe      =  decoupePodsOrSimul(elementsMapStepToPodsData.get(ipods)) ;
				elementsMapStepToPodsDataDecoupe.put(ipods,elementsPodsDecoupe) ;
				List<String>  paramSimul = (elementsMapStepToPodsData.get(ipods)).subList(1,10) ;
				elementsMapStepToPodsData.put(ipods,new ArrayList<String>(paramSimul) ); 
			}
		} catch (FileNotFoundException e) {
			System.out.println("Fichier non accessible ") ; 
			e.getMessage() ; 
			e.printStackTrace();
			System.exit(99);

		} catch (IOException e) {
			System.out.println("Erreur " ) ; 
			e.getMessage() ; 
			e.printStackTrace();
			System.exit(99);
		}
	}
	//----------------------------------------------------------------------------------------------
	public static void  deletetTxtOrCsvFile(String absPathDir, final String prefix, final String  suffix) 
			throws IOException, FileNotFoundException {
		if (new File(absPathDir).exists()) {
			File inputDir = new File(absPathDir)  ; 
			FilenameFilter filterTXT = new FilenameFilter() {	
				public boolean accept(File dir, String name) {
					// TODO Auto-generated method stub
					return (name.startsWith(prefix) && (name.endsWith(suffix)));
				}
			};
			String  fileNames[] = {} ; 
			if (inputDir.isDirectory())  {
				fileNames = inputDir.list(filterTXT)  ; 
				for (String fileName : fileNames) {
					File outputFile = new File(absPathDir + fileName);
					if (outputFile.isFile()) { outputFile.delete(); }
				}
			} ;
		}
	}
	//----------------------------------------------------------------------------------------------
	public static List<String> getListOfCsvFile(String pathDir, final String prefix, final String  suffix) 
			throws IOException, FileNotFoundException {

		// ---- Sélection de la liste des fichiers d'extension suffix et de préfixe prefix ....
		//      dans le répertoire pathDir ...

		List<String> fileNames = new ArrayList<String>()  ; 
		if (new File(pathDir).exists()) {
			File inputDir = new File(pathDir)  ; 
			FilenameFilter filterCSV = new FilenameFilter() {	
				public boolean accept(File dir, String name) {
					// TODO Auto-generated method stub
					return (name.startsWith(prefix) && name.endsWith(suffix));
				}
			};
			String  listeFileNames[] = {} ; 
			if (inputDir.exists() && inputDir.isDirectory())  {
				listeFileNames = inputDir.list(filterCSV)  ; 
				Arrays.sort(listeFileNames);
				fileNames = Arrays.asList(listeFileNames)   ; 
			}
		}
		return fileNames ; 
	}
	//-------------------------------------------------------------------------------------------------------------
	public static void writeOutputTextFile(String pathDir, String fileName, String outputString,Append append) {
		/*
		 * Tester si le répertoire est bien présent 
		 * File outputDir = new File(outputAbsPathDir.toString());
				if (!outputDir.exists() || !outputDir.isDirectory()) {
				outputDir.mkdirs();
				}
		 */
		String outputAbsPathFile = pathDir + fileName ;

		PrintWriter printWriter = null;
		
		try { 
			printWriter = new PrintWriter(new FileWriter(outputAbsPathFile,append.equals(Append.OUI)));
			}
		catch (IOException e) {
			System.out.println("Output file creation error (" + outputAbsPathFile + "),  program aborted in writeOutputTextFile method ! ");
			System.exit(0);
		}
		printWriter.printf(outputString);
		printWriter.flush();
		printWriter.close();
		if (printWriter.checkError()) {
			System.out.println("Output file creation error (" + outputAbsPathFile + "), program aborted in writeOutputTextFile method  ! ");
			System.exit(0);
		}
	}
	//==============================================================================================
	public static void main(String[] args) throws FileNotFoundException, IOException {

		// TODO Auto-generated method stub
		getPodsData();
		initPoids(); 

		List<String> namesSimFiles = getListOfCsvFile(pathFile+"src/files", "simulations_resultats", "csv"); 
		int nbCsvFiles =  namesSimFiles.size() ; 
		
		// Suppression dez fichiers *.txt 
		deletetTxtOrCsvFile(pathFile + "src/files/","result","txt") ; 

		String nameSimFileName = "simulations_resultats01.csv"  ;

		// System.out.println("Traitement du fichier : " + nameSimFileName); 

		CsvReader  productsSim = new CsvReader(pathFile+ "src/files/"+nameSimFileName,';');
		elementsMapStepToSimData.clear(); 
		elementsMapStepToSimDataDecoupe.clear() ; 

		ArrayList<String> elementsSim ;
		Integer stepSimInCurRecord  ;
		while (productsSim.readRecord() ) {
			stepSimInCurRecord       =  Integer.valueOf(productsSim.getValues()[0])  ;
			elementsSim              =  new ArrayList<String>(Arrays.asList(productsSim.getValues())) ; 
			if (elementsMapStepToSimData.containsKey(stepSimInCurRecord)) {
				elementsMapStepToSimData.get(stepSimInCurRecord).addAll(elementsSim) ; 
			}
			else elementsMapStepToSimData.put(stepSimInCurRecord,elementsSim) ;
		}
		productsSim.close() ; 

		ArrayList<String> elementsSimDecoupe  =  new ArrayList<String>() ; 
		for (int isim  : elementsMapStepToSimData.keySet()) {
			elementsSimDecoupe =  decoupePodsOrSimul(elementsMapStepToSimData.get(isim)) ;
			elementsMapStepToSimDataDecoupe.put(isim,elementsSimDecoupe) ;
			List<String>  paramSimul = (elementsMapStepToSimData.get(isim)).subList(1,10) ;
			elementsMapStepToSimData.put(isim,new ArrayList<String>(paramSimul) ); 
		}
		// ---- Calcul des distances
		setMapSSimMoy() ; 
		initSeuils() ; 
		// System.out.println("Seuils : " + seuils) ; 
	}
}


