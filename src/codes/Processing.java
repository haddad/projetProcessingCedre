package codes;


	import java.io.File;
	import java.io.File;
	import java.io.FileInputStream;
	import java.io.FileNotFoundException;
	import java.io.IOException;
	import java.io.FileNotFoundException;
	import java.io.FileWriter;
	import java.io.FilenameFilter;
	import java.io.IOException;
	import java.io.PrintWriter;
	import java.sql.Date;
	import java.lang.* ;
	import java.util.*;
	import java.util.Collections;
	import java.util.ArrayList;
	import java.util.Arrays;
	import java.util.List;
	import java.util.Map;
	import java.util.TreeMap;
	import java.util.Properties;
	import java.util.Enumeration;
	import org.apache.commons.collections.list.TreeList;

	import com.csvreader.CsvReader;
	import com.csvreader.CsvWriter;

	public class Processing {

		public static Locale localeFR = new Locale("fr","French");
		public static String  pathFile = "" ;

		public static Map<Integer,ArrayList<String>> mapStepToPodsData    = new TreeMap<Integer,ArrayList<String>>() ;
		public static Map<Integer,ArrayList<String>> mapStepToPodsParam   = new TreeMap<Integer,ArrayList<String>>() ;
		public static Map<Integer,ArrayList<String>> mapStepToSimData     = new TreeMap<Integer,ArrayList<String>>() ;
		public static Map<Integer,ArrayList<String>> mapStepToSimParam    = new TreeMap<Integer,ArrayList<String>>() ;

		public static Map<Integer,Double> mapSSsimMoy = new TreeMap<Integer,Double>() ;

		public static List<Double>  poids        =  new ArrayList<Double>() ;
		public static ArrayList<Double>  seuils  =  new ArrayList<Double>() ;
		public static ArrayList<Double>  seuils1 =  new ArrayList<Double>() ;


		public static  Properties properties     = null;
		//-------------------------------------------------------------------------------------------------------------------------
		static {
		/* Cette partie du code est exécutée dès le chargement de la classe AbcCedrusMain, avant toute chose */
			try {
				properties = loadProperties(pathFile+"src/files/" + "properties.xml");

			} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//-------------------------------------------------------------------------------------------------------------------------
		public static enum Append {
			OUI, NON ;
		} ;
		public static enum Action {
			DELETE, RENAME ;
		} ;
		//----------------------------------------------------------------------------------------------
		public static  void initPoidsForFile(String fileName, Boolean delete) {

		String outFile = pathFile + "src/files/" +fileName ;

		Calendar calendar = Calendar.getInstance();
	    Integer  seconde  = calendar.get(Calendar.SECOND);
	    Integer  minute   = calendar.get(Calendar.MINUTE);
	    Integer   heure   = calendar.get(Calendar.HOUR_OF_DAY) ;
	    Integer   jour    = calendar.get(Calendar.DAY_OF_YEAR) ;
	    Integer   annee   = calendar.get(Calendar.YEAR) ;

	    String extension = String.format("%4d%03d%02d%02d%02d",annee,jour,heure,minute,seconde) ;
	    // System.out.println(extension);

		try {
			File file =new File(outFile);

    		//if file exists, then delete it
    		if(file.exists()){
    			if (delete ) { file.delete() ;  }
    			else { file.renameTo(new File(outFile + "."+extension)); }
    		}

			CsvWriter csvOutput  = new CsvWriter(new FileWriter(outFile,true), ';');
			for (int i = 1 ; i <=9  ; i++) {
				for (int j = 1 ; j <=18 ; j++) { csvOutput.write("1.0") ; }
				csvOutput.endRecord() ;
			}
			for (int i = 1 ; i <=36  ; i++) { csvOutput.write("1.0") ; }
			csvOutput.endRecord() ;
			csvOutput.close() ;

			} catch (FileNotFoundException e) {
				System.out.println("Fichier non accessible ! ") ;
				e.getMessage() ;
				e.printStackTrace();
				System.exit(99);
			} catch (IOException e) {
				System.out.println("Erreur " ) ;
				e.getMessage() ;
				e.printStackTrace();
				System.exit(99);
			}
		}
		//----------------------------------------------------------------------------------------------
		public static void initPoidsFromFile (String fileName) 	throws IOException, FileNotFoundException  {
			try {
				CsvReader  productsPoids    = new CsvReader(pathFile+"src/files/"+ fileName,';');

				ArrayList<String> elementsPoids ;
				poids.clear() ;

				while ((productsPoids.readRecord())) {
					elementsPoids        =  new ArrayList<String>(Arrays.asList(productsPoids.getValues())) ;
					for (String ipoids : elementsPoids) poids.add(Double.valueOf(ipoids))  ;
				}
				productsPoids.close() ;

			} catch (FileNotFoundException e) {
				System.out.println("Fichier non accessible ! ") ;
				e.getMessage() ;
				e.printStackTrace();
				System.exit(99);
			} catch (IOException e) {
				System.out.println("Erreur " ) ;
				e.getMessage() ;
				e.printStackTrace();
				System.exit(99);
			}
		    /*
			for (int i = 0 ; i < 198 ; i++) {
				poids.add(1.0D) ;
			}
			*/
		}
		//----------------------------------------------------------------------------------------------
		public static void initSeuils (int numFichierTraite, ArrayList<Double> currentSeuils, String monSeuil ) {
			Double seuil ;
			currentSeuils.clear() ;
			Double  ratio    =  Double.valueOf(properties.getProperty("ratio")) ;
			Integer nbSimul  =  Integer.valueOf(properties.getProperty("nbSimul")) ;
			for (int i = 0 ; i < 100 ; i++) {
				seuil = getSeuil(i+1,nbSimul,ratio,numFichierTraite,monSeuil) ;
				currentSeuils.add(seuil) ;
			}
		}

		//----------------------------------------------------------------------------------------------
		@SuppressWarnings("unchecked")
		public static Double getSeuil(int numPods, int nbrSimul, Double ratio, int numFichierTraite,String seuil) {

			ArrayList   concat = new ArrayList() ;
			for (int iSimul = 1 ; iSimul <= nbrSimul ; iSimul++) {
				Double distance = distanceSSobsSSsim(iSimul, numPods) ;
				concat.add(distance) ;
			}

			writeOutputTextFile(pathFile+"src/files/","concat"+String.valueOf(numFichierTraite)+".txt",concat.toString()+"\n",Append.OUI)  ;
			// System.out.println(concat) ;

			Collections.sort(concat);

			// writeOutputTextFile(pathFile+"src/files/","concatrie"+String.valueOf(numFichierTraite)+"-"+seuil+ ".txt",concat.toString()+"\n",Append.OUI)  ;
			// System.out.println(concat) ;

			int indiceSeuil = new Double(nbrSimul*ratio).intValue();
			return (Double) concat.get(indiceSeuil) ;
		}
		//-----------------------------------------------------------------------------------------------
		public static Double calculSSsimMoy(int k ) {
			int iSim = 1 ;
			int    l = 1 ;
			Double somme = 0D ;
			int taille =  (mapStepToSimData.keySet()).size() ;
			while ( (iSim <= taille)  && (l <= 1000 ) ) {
				ArrayList<String> elementsSimul = mapStepToSimData.get(iSim) ;
				if ( ! ((elementsSimul.get(k)).contentEquals("NAN") )) {
					somme = somme + (Double.valueOf(elementsSimul.get(k))) ;
					l++ ;
				}
			iSim++ ;
			}
			// if (iSim >= 100000) { System.out.println("isim,l = " + iSim + "," + l + " Somme = " + somme/1000) ; } ;
			return somme/1000  ;
		}
		//----------------------------------------------------------------------------------------------
		public static void setMapSSimMoy() {
			for (int i = 0 ; i < poids.size() ; i++) {
				mapSSsimMoy.put(i,calculSSsimMoy(i)) ;
			}
		}
		//----------------------------------------------------------------------------------------------
		public  static Properties loadProperties(String fileName)
			throws IOException, FileNotFoundException {

			properties = new Properties();
			FileInputStream input = new FileInputStream(fileName);
			try {
				properties.loadFromXML(input);
			}
			catch (IOException e) {
				System.out.println("Le fichier XML des paramètres (ou des masques) est absent ou corrompu ! ");
				properties = null;
			}
			finally {
				input.close();
			}
			return properties;
		}
		//----------------------------------------------------------------------------------------------
		public static Double distanceSSobsSSsim(int iSim, int jPods ) {

			Double distance =  0.0D ;
			Double trucSomme = 0.0D ;

			ArrayList<String> elementsPods  = mapStepToPodsData.get(jPods)  ;
			ArrayList<String> elementsSimul = mapStepToSimData.get(iSim)  ;

			int taillePoids = poids.size() ;
			for (int k = 0 ; k < taillePoids ; k++ ) {
				String curValPods = elementsPods.get(k) ;
				String curValSim  = elementsSimul.get(k) ;
				Double valPods    = 0.0D ;
				Double valSim     = 0.0D ;

				if (curValPods.contentEquals("NAN")) { trucSomme = 0D ; }
				else {
					valPods = Double.valueOf(curValPods);
					if (curValSim.contentEquals("NAN")) {
						// valSim    = mapSSsimMoy.get(k)  ;
						trucSomme = Math.pow((valPods - mapSSsimMoy.get(k) ),2) / poids.get(k) ;
					}
					else {
						// valSim    =  Double.valueOf(curValSim) ;
						trucSomme =  Math.pow((valPods - Double.valueOf(curValSim)),2) / poids.get(k) ;
					}
				}
				distance = distance + trucSomme   ;
			}
			// System.out.println(" Distance =" + distance) ;
			return distance ;
		}
		//-------------------------------------------------------------------------------------------
		@SuppressWarnings("unchecked")
		public static void getPodsData() {
			try {
				CsvReader  productsPods        = new CsvReader(pathFile+"src/files/pods_resultats.csv",';');
				String[]   valuesPodsInLine        ;
				Integer    stepPodsInCurRecord     ;
				ArrayList<String> elementsPods ;
				ArrayList<String> elementsPodsDecoupe ;

				mapStepToPodsData.clear() ;
				mapStepToPodsParam.clear() ;

				while ((productsPods.readRecord())) {
					// stepPodsInCurRecord =  Integer.valueOf(productsPods.getValues()[0]) ;
					elementsPods        =  new ArrayList<String>(Arrays.asList(productsPods.getValues())) ;
					stepPodsInCurRecord =  Integer.valueOf(elementsPods.get(0)) ;

					if (mapStepToPodsData.containsKey(stepPodsInCurRecord)) {
						if (elementsPods.size() == 20) {
							// Traitement d'une ligne de polygone
							// List<String>  polygonData = elementsPods.subList(2,20) ;
							(mapStepToPodsData.get(stepPodsInCurRecord)).addAll(elementsPods.subList(2,20));
						}
						else {
							// Traitement de la ligne des FST
							for (int i=0 ;  i< 106 ;  i=i+3 ) {
								(mapStepToPodsData.get(stepPodsInCurRecord)).addAll(elementsPods.subList(i,i +1)) ;
							}
						}
					}
					else {
						    List<String>  paramPods = elementsPods.subList(1,10) ;
						    mapStepToPodsParam.put(stepPodsInCurRecord,new ArrayList<String>(paramPods)) ;
							mapStepToPodsData.put(stepPodsInCurRecord,new ArrayList<String>()) ;
					}
				}
				productsPods.close() ;

			} catch (FileNotFoundException e) {
				System.out.println("Fichier non accessible ") ;
				e.getMessage() ;
				e.printStackTrace();
				System.exit(99);
			} catch (IOException e) {
				System.out.println("Erreur " ) ;
				e.getMessage() ;
				e.printStackTrace();
				System.exit(99);
			}
		}
		//----------------------------------------------------------------------------------------------
		public static void  deletetTxtOrCsvFile(String absPathDir, final String prefix, final String  suffix, Action deleteRename)
				throws IOException, FileNotFoundException {
			if (new File(absPathDir).exists()) {
				File inputDir = new File(absPathDir)  ;
				FilenameFilter filterTXT = new FilenameFilter() {
					public boolean accept(File dir, String name) {
						// TODO Auto-generated method stub
						return (name.startsWith(prefix) && (name.endsWith(suffix)));
					}
				};
				//----------
				String  fileNames[] = {} ;
				if (inputDir.isDirectory())  {
				fileNames = inputDir.list(filterTXT)  ;
				for (String fileName : fileNames) {
					File outputFile    = new File(absPathDir + fileName);
					if (outputFile.isFile()) {
						if  (deleteRename.equals(Action.RENAME) )
						     { outputFile.renameTo(new File(absPathDir + fileName+".save"))  ;  }
						else {  outputFile.delete();  }
					}
				}
			} ;
				//----------
				/*
				String  fileNames[] = {} ;
				if (inputDir.isDirectory())  {
					fileNames = inputDir.list(filterTXT)  ;
					for (String fileName : fileNames) {
						File outputFile = new File(absPathDir + fileName);
						if (outputFile.isFile()) { outputFile.delete(); }
					}
				} ;
				*/
			}
		}
		//----------------------------------------------------------------------------------------------
		public static List<String> getListOfCsvFile(String pathDir, final String prefix, final String  suffix)
				throws IOException, FileNotFoundException {

			// ---- Sélection de la liste des fichiers d'extension "suffix"
			//      et de préfixe "prefix", dans le répertoire pathDir ...

			List<String> fileNames = new ArrayList<String>()  ;

			if (new File(pathDir).exists()) {
				File inputDir = new File(pathDir)  ;
				FilenameFilter filterCSV = new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return (name.startsWith(prefix) && name.endsWith(suffix));
					}
				};
				String  listeFileNames[] = {} ;
				if (inputDir.exists() && inputDir.isDirectory())  {
					listeFileNames = inputDir.list(filterCSV)  ;
					Arrays.sort(listeFileNames);
					fileNames = Arrays.asList(listeFileNames)   ;
				}
			}
			return fileNames ;
		}
		//-------------------------------------------------------------------------------------------------------------
		public static void writeOutputTextFile(String pathDir, String fileName, String outputString,Append append) {

			 // Utilisation : writeOutputTextFile(pathFile+"src/files/","calculs_resultats"+String.format("%03d",j)+".csv",buffer.toString(),Append.OUI);
			 /* Tester si le répertoire est bien présent
			 * File outputDir = new File(outputAbsPathDir.toString());
					if (!outputDir.exists() || !outputDir.isDirectory()) {
					outputDir.mkdirs();
					}
			 */
			String outputAbsPathFile = pathDir + fileName ;

			PrintWriter printWriter = null;
			try {
				printWriter = new PrintWriter(new FileWriter(outputAbsPathFile,append.equals(Append.OUI)));
			}
			catch (IOException e) {
				System.out.println("Output file creation error (" + outputAbsPathFile + "),  program aborted in writeOutputTextFile method ! ");
				System.exit(0);
			}
			printWriter.printf(outputString);
			printWriter.flush();
			printWriter.close();

			if (printWriter.checkError()) {
				System.out.println("Output file creation error (" + outputAbsPathFile + "), program aborted in writeOutputTextFile method  ! ");
				System.exit(0);
			}
		}
		//----------------------------------------------------------------------------------------------
		public static void printProperties () {
			Enumeration enuKeys = properties.keys();
			while (enuKeys.hasMoreElements()) {
					String key = (String) enuKeys.nextElement();
					String value = properties.getProperty(key);
					System.out.println(key + " : " + value);
			}
		}

		//==============================================================================================
		public static void main(String[] args) throws IOException, FileNotFoundException  {

			Locale.setDefault(new Locale("FRENCH"));

			/*  Réinitialiser le fichier poids à la valeur de 1.0 
			 *  initPoidsForFile(properties.getProperty("fichierPoids"),Boolean.FALSE);
			 *  deletetTxtOrCsvFile(pathFile + "src/files/","concat","txt",Action.DELETE) ;
			*/

			initPoidsFromFile(properties.getProperty("fichierPoids"))  ;
			getPodsData();

			try {
				List<String> namesSimFiles = getListOfCsvFile(pathFile+"src/files", "simulations_resultats", "csv");
				int nbCsvFiles =  namesSimFiles.size() ;
				
				// Suppression des fichiers résultats anciens (issus d'une session précédente).
				deletetTxtOrCsvFile(pathFile + "src/files/","calculs_resultats","csv",Action.DELETE) ;

				ArrayList<String> elementsSim = null ;  ;

				for (int i = 0 ; i < nbCsvFiles ; i++ )  {

					// System.out.println("Traitement du fichier : " + namesSimFiles.get(i));

					CsvReader  productsSim = new CsvReader(pathFile+ "src/files/"+namesSimFiles.get(i),';');

					mapStepToSimData.clear() ;
					mapStepToSimParam.clear() ;

					Integer stepSimInCurRecord  ;
					while (productsSim.readRecord() ) {
						elementsSim              =  new ArrayList<String>(Arrays.asList(productsSim.getValues())) ;
						stepSimInCurRecord       =  Integer.valueOf(elementsSim.get(0)) ;

						if (mapStepToSimData.containsKey(stepSimInCurRecord)) {
								if (elementsSim.size() == 20) {
									// Traitement d'une ligne de polygone
									// List<String>  polygonData = elementsSim.subList(2,20) ;
									(mapStepToSimData.get(stepSimInCurRecord)).addAll(elementsSim.subList(2,20));
								}
								else {
									// Traitement de la ligne des FST
									for (int ind=0 ;  ind<106 ;  ind=ind+3 ) {
									(mapStepToSimData.get(stepSimInCurRecord)).addAll(elementsSim.subList(ind,ind +1)) ;
								}
							}
						}
						else {
							List<String>  paramSim = elementsSim.subList(1,10) ;
							mapStepToSimParam.put(stepSimInCurRecord,new ArrayList<String>(paramSim)) ;
							mapStepToSimData.put(stepSimInCurRecord,new ArrayList<String>()) ;
						}
					}
					productsSim.close() ;

					// ==================================  Calcul des distances  ======================================================
					Double distance = 0D ;

					if (i == 0) {
						setMapSSimMoy() ;
						initSeuils(i,seuils,"seuilZero") ;
					}

					if (i==1 ) { initSeuils(i,seuils1,"seuilUn") ; }

					for (int k = 1 ; k <= 100000 ; k++) {
						int numSimul =  i*100000 + k ;
						String chaineNumSimul = String.format("%07d",numSimul) ;

						for (int j = 1 ; j <= 100 ; j++) {

							 distance = distanceSSobsSSsim(k,j) ;

							// --------------   Enregistrements des valeurs de distance convenable

							 if (distance <= seuils.get(j-1)) {
								CsvWriter csvOutput  = new CsvWriter(new FileWriter(pathFile+"src/files/"+"calculs_resultats"+String.format("%03d",j)+".csv", true), ';');

								// ---------------- Enregistrer les paramètres de la simulation
								csvOutput.write(chaineNumSimul) ;
								ArrayList<String>   paramSimul =  mapStepToSimParam.get(k) ;
								for (int m =0 ; m < paramSimul.size(); m++) {
									csvOutput.write((paramSimul.get(m)).replaceAll("0{0,4}E\\+00$", "")) ;
								}
								csvOutput.endRecord() ;
								// ---------------- Enregistrer les données de la simulation
								csvOutput.write(chaineNumSimul) ;
								ArrayList<String> dataSimul =  mapStepToSimData.get(k) ;
								for (int m =0 ; m < dataSimul.size(); m++) {
									csvOutput.write((dataSimul.get(m)).replaceAll("0{0,4}E\\+00$", "")) ;
								}
								csvOutput.endRecord() ;
								// ----------------- Enregistrer la distance
								csvOutput.write(chaineNumSimul) ;
								csvOutput.write((String.format("%.5E",distance)).replaceAll("0{0,4}E\\+00$", "")) ;
								csvOutput.endRecord() ;

								csvOutput.close() ;

								// .replaceAll(",","\\.")) ;
							 }
						}
					}
				}
			} catch (FileNotFoundException e) {
				System.out.println("Fichier non accessible ! ") ;
				e.getMessage() ;
				e.printStackTrace();
				System.exit(99);
			} catch (IOException e) {
				System.out.println("Erreur " ) ;
				e.getMessage() ;
				e.printStackTrace();
				System.exit(99);
			}
		}
	}


