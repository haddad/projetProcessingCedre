package codes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

public class TestString {
	
	public static String  pathFile = "/home/haddad/workspaceUn/projetPostTraitement/" ; 
	public static Map<Integer,ArrayList<String>> elementsMapStepToSimData    = new TreeMap<Integer,ArrayList<String>>()  ;
	public static Map<Integer,ArrayList<String>> elementsMapStepToPodsData   = new TreeMap<Integer,ArrayList<String>>() ; 
	public static List<Double>  weight                                       = new ArrayList<Double>(198) ;
	public static String[]      valuesSimInLine ;
	
	
	static public String customFormat(String pattern, double value ) {
	      DecimalFormat myFormatter = new DecimalFormat(pattern);
	      String output = myFormatter.format(value);
	      // System.out.println(value + "  " + pattern + "  " + output);
	      return output ; 
	   }
	
	public static ArrayList<String> elementsSim ; // = new ArrayList<String>() ; 

	public static void getPodsData() {
		// NumberFormat myFormat = DecimalFormat.getInstance(Locale.ENGLISH);
		Locale locale  = new Locale("FRENCH") ; 
		Locale.setDefault(locale); 

		// myFormat.setMinimumFractionDigits(1); //Nb de Digit mini
		// myFormat.setMaximumFractionDigits(5); //Nb de Digit Maxi

		try {
			CsvReader  productsPods        = new CsvReader("src/files/pods_resultats.csv",';');
			String[]   valuesPodsInLine        ;    
			Integer    stepPodsInCurRecord = 1 ;
			ArrayList<String> elementsPods ; 

			while ((productsPods.readRecord())) {
				stepPodsInCurRecord =  Integer.valueOf(productsPods.getValues()[0]) ; 
			   // valuesPodsInLine =  productsPods.getValues() ;

				elementsPods  =  new ArrayList<String>(Arrays.asList(productsPods.getValues())) ; 
				
				// System.out.println("Nb elements : " + elementsPods.size()); 
				// elementsPods.remove(0) ; // Supprimer la valeur du step
				// System.out.println( elementsPods);  

				if (elementsMapStepToPodsData.containsKey(stepPodsInCurRecord)) {
					elementsMapStepToPodsData.get(stepPodsInCurRecord).addAll(elementsPods); 
				}
				else elementsMapStepToPodsData.put(stepPodsInCurRecord,elementsPods) ;
			}
			productsPods.close() ;

			System.out.println("Ici : " + elementsMapStepToPodsData.get(1).size() ); 
			elementsPods  = elementsMapStepToPodsData.get(1) ; 
			// elementsPods.remove(1) ;
			List<String> sousListe = elementsPods.subList(1,10) ; 
			System.out.println("Param : " + sousListe.size()); 
			
			
			sousListe = elementsPods.subList(10,30)  ; 
		 // Paramètres (step, valeurs - 9 - )
			// System.out.println(elementsPods.subList(1,10)) ; 
			// --------------------------------- Valeurs liées aux polygones
			System.out.println(elementsPods.subList(12,30) ) ; 
			System.out.println(sousListe.subList(2,20));
			System.out.println(elementsPods.subList(32,50).size()) ;
			System.out.println(elementsPods.subList(52,70).size()) ;
			System.out.println(elementsPods.subList(72,90).size()) ;
			System.out.println(elementsPods.subList(92,110).size()) ;
			System.out.println(elementsPods.subList(112,130).size()) ;
			System.out.println(elementsPods.subList(132,150).size()) ; 
			System.out.println(elementsPods.subList(152,170).size()) ;
			System.out.println(elementsPods.subList(172,190).size()) ;
			// ------------------ Valeurs  FST
			
			sousListe = elementsPods.subList(193, elementsPods.size()); ; 
			
			for (int i=0 ;  i< 106 ;  i=i+3 ) {
				// System.out.print(sousListe.subList(i, i +1)) ; 
				System.out.println(sousListe.get(i));
			}
			
			System.out.println(); ; 
			List<String> ssliste = new ArrayList<String>() ; 
			
			for (int i=0 ;  i< 106 ;  i=i+3 ) {
				ssliste.addAll(elementsPods.subList(193 + i, 193 + i +1)) ; 
			}
			System.out.println(ssliste  ); 
			 
		 /*   
		    for (int i = 0 ; i < sousListe.size() ; i++ ) {
				String curValOr = sousListe.get(i) ;
				String curVal = curValOr.replace("E+00", "");
				// curVal = curVal.replaceAll(",", "\\.");
				Double cur     = Double.valueOf(curVal); 
				String curBis = customFormat("#.#####", cur);
				// System.out.println("Val courante : " + curBis + " " + cur  + " " + cur.toString() + " " + String.format("%1.5E", cur) + " "  + curVal );
				System.out.println("Val courante curVal : " + curBis + " " +  curValOr + " " + curValOr.replace("E+00", "")) ; 
			}
		    */
			// System.out.println("Ici : " + sousListe ); 

		} catch (FileNotFoundException e) {
			System.out.println("Fichier non accessible ") ; 
			e.getMessage() ; 
			e.printStackTrace();
			System.exit(99);

		} catch (IOException e) {
			System.out.println("Erreur " ) ; 
			e.getMessage() ; 
			e.printStackTrace();
			System.exit(99);
		}
	}
	
	public static  void initPoidsForFile(String fileName, Boolean delete) {
		
		String outFile = pathFile + "src/files/" +fileName ; 
		
		Calendar calendar = Calendar.getInstance();
	    Integer  seconde  = calendar.get(Calendar.SECOND);
	    Integer  minute   = calendar.get(Calendar.MINUTE);
	    Integer   heure   = calendar.get(Calendar.HOUR_OF_DAY) ;
	    Integer   jour    = calendar.get(Calendar.DAY_OF_YEAR) ; 
	    Integer   annee   = calendar.get(Calendar.YEAR) ; 
	    
	    String extension = String.format("%4d%d%d%d%d",annee,jour,heure,minute,seconde) ; 
	    // System.out.println(extension);  

		try {
			File file =new File(outFile);
    		
    		//if file exists, then delete it
    		if(file.exists()){
    			if (delete ) { file.delete() ;  }
    			else { file.renameTo(new File(outFile + "."+extension)); }
    		} 
   
			CsvWriter csvOutput  = new CsvWriter(new FileWriter(outFile,true), ';');
			for (int i = 1 ; i <=9  ; i++) {
				for (int j = 1 ; j <=18 ; j++) {
					csvOutput.write("1.0") ;
				}
			csvOutput.endRecord() ;
			}
			for (int i = 1 ; i <=36  ; i++) {
				csvOutput.write("1.0") ;
			}
			csvOutput.endRecord() ;
			csvOutput.close() ;

		} catch (FileNotFoundException e) {
			System.out.println("Fichier non accessible ! ") ;
			e.getMessage() ;
			e.printStackTrace();
			System.exit(99);
		} catch (IOException e) {
			System.out.println("Erreur " ) ;
			e.getMessage() ;
			e.printStackTrace();
			System.exit(99);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//  getPodsData() ;9.80100E+00;5;9.59531E+00;3.67200E-01;1.80854E+02;2.86272E-01;1.42778E+03;8.03845E+00;414
		String chaine = new String("9.80100E+00") ;
		String chaineBis = chaine.replaceAll("0{0,4}E\\+00$", "") ; 
		System.out.println(chaineBis) ;
		
		int  un = 20 ; 
		String cu = String.valueOf(un) ; 
		
		System.out.println("U = " + un);
		// initPoidsForFile("poids.csv",Boolean.TRUE );
		
		// initPoidsForFile("poids.txt",Boolean.FALSE);
		
	}

}
